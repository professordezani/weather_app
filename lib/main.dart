// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(WeatherApp());
}

const titleStyle = TextStyle(
  color: Colors.white,
  fontSize: 14,
  fontWeight: FontWeight.w500,
);

class WeatherApp extends StatelessWidget {

  Future<Map<String, dynamic>> getWeather() async {
    String url = "http://api.weatherapi.com/v1/forecast.json?key=63487048652c401aa6e213642242703&q=-20.78836795179825,%20-49.40198988420008&days=1&aqi=no&alerts=no";

    var response = await http.get(Uri.parse(url));

    if(response.statusCode == 200) {
      return json.decode(response.body);
    }

    throw Exception(response.body);
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xFF255AF4),
        body: FutureBuilder<Map<String, dynamic>>(
          future: getWeather(),
          builder: (context, snapshot) {

            if(!snapshot.hasData) {
              return CircularProgressIndicator();
            }

            var weather = snapshot.data!;

            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  weather['location']['name'],
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.network('https:${weather['current']['condition']['icon']}', width: 128, height: 128, fit: BoxFit.fill),
                    Text(
                      weather['current']['condition']['text'],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 36,
                      ),
                    ),
                    Text(
                      '${weather['current']['temp_c']}°C',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 64,
                        fontWeight: FontWeight.w500,
                        shadows: [
                          Shadow(
                            color: Colors.black,
                            offset: Offset(4, 4),
                            blurRadius: 7,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Item("umidade.png", "Humidity", "${weather['current']['humidity']}%"),
                    Item("vento.png", "Wind", "${weather['current']['wind_kph']}km/h"),
                    Item("sensacao.png", "Feels like", "${weather['current']['feelslike_c']}°C"),
                  ],
                ),
                Container(
                  height: 130,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      forecast("11AM", "chuva", "35"),
                      forecast("12PM", "sol", "35"),
                      forecast("01PM", "chuva", "35"),
                      forecast("02PM", "nublado", "35"),
                      
                    ],
                  ),
                ),
              ],
            );
          }
        ),
      ),
    );
  }

  Container forecast(String time, String image, String temp) {
    return Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        time,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Image.asset(
                        'images/$image.png',
                        width: 36,
                        height: 36,
                      ),
                      Text(
                        "$temp°C",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                );
  }
}

class Item extends StatelessWidget {
  final String image;
  final String text;
  final String value;

  Item(this.image, this.text, this.value);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset('images/$image'),
        Text(
          this.text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(this.value,
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            )),
      ],
    );
  }
}
